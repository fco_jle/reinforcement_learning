from reinforcement_learning.lazy.bandits_greedy import run_experiment_decaying_epsilon as regreedy
from reinforcement_learning.lazy.bandits_optimistic import run_experiment as reopt
from reinforcement_learning.lazy.bandits_ucb import run_experiment as reucb
from reinforcement_learning.lazy.bandits_bayesian import run_experiment as rebayes
import matplotlib.pyplot as plt

if __name__ == '__main__':
    eps = regreedy(1, 2, 3, 100000)
    oiv = reopt(1, 2, 3, 100000)
    ucb = reucb(1, 2, 3, 100000)
    bayes = rebayes(1, 2, 3, 100000)

    plt.plot(eps, label='Decaying Epsilon')
    plt.plot(oiv, label='Optimistic')
    plt.plot(ucb, label='UCB1')
    plt.plot(bayes, label='Bayesian')
    plt.legend()
    plt.xscale('log')
    plt.show()
