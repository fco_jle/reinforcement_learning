{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#The-Agent–Environment-Interface\" data-toc-modified-id=\"The-Agent–Environment-Interface-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>The Agent–Environment Interface</a></span></li><li><span><a href=\"#Goals-and-Rewards\" data-toc-modified-id=\"Goals-and-Rewards-2\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>Goals and Rewards</a></span></li><li><span><a href=\"#Returns-and-Episodes\" data-toc-modified-id=\"Returns-and-Episodes-3\"><span class=\"toc-item-num\">3&nbsp;&nbsp;</span>Returns and Episodes</a></span></li><li><span><a href=\"#Policies-and-Value-Functions\" data-toc-modified-id=\"Policies-and-Value-Functions-4\"><span class=\"toc-item-num\">4&nbsp;&nbsp;</span>Policies and Value Functions</a></span></li><li><span><a href=\"#Optimal-Policies-and-Optimal-Value-Functions\" data-toc-modified-id=\"Optimal-Policies-and-Optimal-Value-Functions-5\"><span class=\"toc-item-num\">5&nbsp;&nbsp;</span>Optimal Policies and Optimal Value Functions</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Finite Markov Decision Processes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We introduce the formal problem of finite Markov decision processes, or\n",
    "finite MDPs, which we try to solve in the rest of the book. This problem involves evaluative feedback, as in bandits, but also an associative aspect—choosing different actions in\n",
    "different situations. MDPs are a classical formalization of sequential decision making,\n",
    "where actions influence not just immediate rewards, but also subsequent situations, or\n",
    "states, and through those future rewards. Thus MDPs involve delayed reward and the\n",
    "need to tradeoff immediate and delayed reward. Whereas in bandit problems we estimated\n",
    "the value $q_*(a)$ of each action a, in MDPs we estimate the value $q_*(s, a)$ of each action a\n",
    "in each state s, or we estimate the value $v_*(s)$ of each state given optimal action selections.\n",
    "These state-dependent quantities are essential to accurately assigning credit for long-term\n",
    "consequences to individual action selections."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Agent–Environment Interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "MDPs are meant to be a straightforward framing of the problem of learning from\n",
    "interaction to achieve a goal. The learner and decision maker is called the agent. The\n",
    "thing it interacts with, comprising everything outside the agent, is called the environment.\n",
    "These interact continually, the agent selecting actions and the environment responding to these actions and presenting new situations to the agent.1 The environment also gives\n",
    "rise to rewards, special numerical values that the agent seeks to maximize over time\n",
    "through its choice of actions.\n",
    "\n",
    "At each time step t, the agent receives some representation\n",
    "of the environment’s state, $S_t\\in\\mathcal{S}$, and on that basis selects an action, $A_t\\in\\mathcal{A}(s)$.3 One\n",
    "time step later, in part as a consequence of its action, the agent receives a numerical\n",
    "reward, $R_{t+1}\\in\\mathcal{R}\\in\\mathbb{R}$, and finds itself in a new state,$S_{t+1}$ The MDP and agent\n",
    "together thereby give rise to a sequence or trajectory.\n",
    "\n",
    "In a finite MDP, the sets of states, actions, and rewards all have a finite\n",
    "number of elements. In this case, the random variables $R_t$ and $S_t$ have well defined\n",
    "discrete probability distributions dependent only on the preceding state and action. That\n",
    "is, for particular values of these random variables, there is a probability\n",
    "of those values occurring at time t, given particular values of the preceding state and\n",
    "action:\n",
    "\n",
    "$$ p(s', r|s, a) = Pr\\{S_t=s', R_t=r|S_{t-1}=s, A_{t-1}=a\\}$$\n",
    "\n",
    "The function p defines the dynamics of the MDP. In a Markov decision process, the probabilities given by p completely characterize the\n",
    "environment’s dynamics. That is, the probability of each possible value for $S_t$ and $R_t$\n",
    "depends only on the immediately preceding state and action and, given\n",
    "them, not at all on earlier states and actions. This is best viewed a restriction not on the\n",
    "decision process, but on the state. The state must include information about all aspects\n",
    "of the past agent–environment interaction that make a di↵erence for the future. If it\n",
    "does, then the state is said to have the Markov property.\n",
    "\n",
    "From the four-argument dynamics function, p, one can compute anything else one might\n",
    "want to know about the environment, such as the state-transition probabilities:\n",
    "\n",
    "$$ p(s'|s,a) = \\sum_{r\\in\\mathcal{R}}p(s', r|s,a) $$\n",
    "\n",
    "We can also compute the expected rewards for state–action pairs:\n",
    "\n",
    "$$r(s,a) = \\sum_{r\\in\\mathcal{R}}r\\sum_{s'\\in\\mathcal{S}}p(s',r|s, a)$$\n",
    "\n",
    "and the expected rewards for state–action–next-state triples:\n",
    "\n",
    "$$ r(s, a, s') = \\sum_{r\\in\\mathcal{R}}r \\frac{p(s', r|s, a)}{p(s'|s, a)} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Goals and Rewards"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In reinforcement learning, the purpose or goal of the agent is formalized in terms of a\n",
    "special signal, called the reward, passing from the environment to the agent. At each time\n",
    "step, the reward is a simple number. Informally, the agent’s goal is to maximize\n",
    "the total amount of reward it receives. This means maximizing not immediate reward,\n",
    "but cumulative reward in the long run. We can clearly state this informal idea as the\n",
    "reward hypothesis:\n",
    "\n",
    "That all of what we mean by goals and purposes can be well thought of as\n",
    "the maximization of the expected value of the cumulative sum of a received\n",
    "scalar signal (called reward)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2019-10-09T20:31:11.636916Z",
     "start_time": "2019-10-09T20:31:11.633916Z"
    }
   },
   "source": [
    "## Returns and Episodes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have said that the\n",
    "agent’s goal is to maximize the cumulative reward it receives in the long run.\n",
    "In general, we seek to maximize the expected return, where the return, denoted $G_t$, is\n",
    "defined as some specific function of the reward sequence. In the simplest case the return\n",
    "is the sum of the rewards:\n",
    "\n",
    "$$ G_t = R_{t+1}+R_{t+2}+R_{t+3}+...+R_{T} $$\n",
    "\n",
    "Where T is a final time step. This approach makes sense in applications in which there\n",
    "is a natural notion of final time step, that is, when the agent–environment interaction\n",
    "breaks naturally into subsequences, which we call episodes. Each episode ends in a special\n",
    "state called the terminal state, followed by a reset to a standard starting state. Tasks\n",
    "with episodes of this kind are called episodic tasks. In episodic tasks we sometimes need\n",
    "to distinguish the set of all nonterminal states, denoted $S$, from the set of all states plus\n",
    "the terminal state, denoted $S^+$. The time of termination, T, is a random variable that\n",
    "normally varies from episode to episode.\n",
    "\n",
    "On the other hand, in many cases the agent–environment interaction does not break\n",
    "naturally into identifiable episodes, but goes on continually without limit.We call these continuing tasks. The previous definition of expected reward is problematic in this cases, so we use a slightly more complex definition: \n",
    "\n",
    "$$ G_t = R_{t+1}+\\gamma R_{t+1} + \\gamma^2 R_{t+3}+... = \\sum_{k=0}^\\infty \\gamma^k R_{t+k+1} $$\n",
    "\n",
    "where $\\gamma$ is a parameter, $0\\leq\\gamma\\leq 1$, called the discount rate. If $\\gamma=0$, the agent is “myopic” in being concerned only with maximizing immediate rewards. As $\\gamma$ approaches 1, the return objective takes future rewards into\n",
    "account more strongly; the agent becomes more farsighted.\n",
    "\n",
    "Returns at successive time steps are related to each other in a way that is important\n",
    "for the theory and algorithms of reinforcement learning:\n",
    "\n",
    "$$G_t = R_{t+1}+\\gamma G_{t+1}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Policies and Value Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Almost all reinforcement learning algorithms involve estimating value functions—functions of states (or of state–action pairs) that estimate how good (in terms of expected return) it is for the agent to be in a given state (or how good it is to perform a given action in a given state). Accordingly, value functions are defined with respect to particular ways of acting, called policies.\n",
    "\n",
    "A policy is a mapping from states to probabilities of selecting each possible action. If the agent is following policy $\\pi$ at time t, then $\\pi(a|s)$ is the probability that $A_t = a$ if $S_t = s$.\n",
    "\n",
    "The value function of a state s under a policy $\\pi$, denoted $v_{\\pi}(s)$, is the expected return when starting in s and following $\\pi$ thereafter. For MDPs, we can define $v_{\\pi}$ (state-value function for policy $\\pi$) formally by:\n",
    "\n",
    "$$v_\\pi(s) = \\mathbb{E}_{\\pi}[G_t|S_t=s]=\\mathbb{E}_\\pi\\bigg[\\sum_{k=0}^{\\infty}\\gamma^kR_{t+k+1}|S_t=s\\bigg], \\forall s \\in \\mathcal{S}$$\n",
    "\n",
    "Similarly, we define the value of taking action a in state s under a policy $\\pi$ as:\n",
    "\n",
    "$$q_\\pi(s,a) = \\mathbb{E}_{\\pi}[G_t|S_t=s, A_t=a]=\\mathbb{E}_\\pi\\bigg[\\sum_{k=0}^{\\infty}\\gamma^kR_{t+k+1}|S_t=s, A_t=a\\bigg], \\forall s \\in \\mathcal{S}$$\n",
    "\n",
    "A fundamental property of value functions used throughout reinforcement learning and dynamic programming is that they satisfy recursive relationships similar to that which we have already established for the return. For any policy $\\pi$ and any state s, the following consistency condition holds between the value of s and the value of its possible successor states:\n",
    "\n",
    "$$ v_\\pi(s) = \\sum_a\\pi(a|s)\\sum_{s', r}p(s',r|s,a)[r+\\gamma v_{\\pi}(s')], \\forall s\\in\\mathcal{S} $$\n",
    "\n",
    "This equation is the Bellman equation for $v_\\pi$. It expresses a relationship between the value of a state and the values of its successor states.\n",
    "\n",
    "The value function $v_\\pi$ is the unique solution to its Bellman equation. We will see later how this Bellman equation forms the basis of a number of ways to compute, approximate, and learn $v_\\pi$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimal Policies and Optimal Value Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A policy $\\pi$ is\n",
    "defined to be better than or equal to a policy $\\pi'$ if its expected return is greater than\n",
    "or equal to that of $\\pi'$ for all states. There is always at least one policy that is better than or equal to all other\n",
    "policies. This is an optimal policy. Although there may be more than one, we denote all\n",
    "the optimal policies by $\\pi_*$ They share the same state-value function, called the optimal\n",
    "state-value function, denoted $v_*$ and defined as:\n",
    "\n",
    "$$ v_*(s) = \\max_\\pi v_{\\pi}(s) $$\n",
    "\n",
    "Optimal policies also share the same optimal action-value function, denoted $q_*$, and\n",
    "defined as\n",
    "\n",
    "$$ q_*(s|a) = \\max_\\pi q_{\\pi}(s,a) $$\n",
    "\n",
    "Ee can write $q_*$ in terms of $v_*$ as follows:\n",
    "\n",
    "$$ q_*(s,a) = \\mathbb{E}[R_{t+1}+\\gamma v_*(S_{t+1})|S_t=s, A_t=a] $$\n",
    "\n",
    "Because $v_*$ is the value function for a policy, it must satisfy the self-consistency\n",
    "condition given by the Bellman equation for state values. Because it is the optimal\n",
    "value function, however, $v_*$ consistency condition can be written in a special form\n",
    "without reference to any specific policy. This is the Bellman equation for $v_*$, or the\n",
    "Bellman optimality equation. Intuitively, the Bellman optimality equation expresses the fact that the value of a state under an optimal policy must equal the expected return for the best action from that state\n",
    "\n",
    "$$ v_*(s)=\\max_a \\sum_{s', r}p(s',r|s,a)[r+\\gamma\\max_{a'}q_*(s',a')] $$\n",
    "\n",
    "For finite MDPs, the Bellman optimality equation for $v_*$ has a unique solution. The Bellman optimality equation is actually a system of equations, one for each state, so if there are n states, then there are n equations in n unknowns. If the dynamics p of the environment are known, then in principle one can solve this system of equations for $v_*$ using any one of a variety of methods for solving systems of nonlinear equations. One can solve a related set of equations for $q_*$.\n",
    "\n",
    "Once one has $v_*$, it is relatively easy to determine an optimal policy. For each state s, there will be one or more actions at which the maximum is obtained in the Bellman optimality equation. Any policy that assigns nonzero probability only to these actions is an optimal policy. You can think of this as a one-step search. If you have the optimal value function, $v_*$, then the actions that appear best after a one-step search will be optimal actions. Another way of saying this is that any policy that is greedy with respect to the optimal evaluation function $v_*$ is an optimal policy. The term greedy is used in computer science to describe any search or decision procedure that selects alternatives based only on local or immediate considerations, without considering the possibility that such selection may prevent future access to even better alternatives. Consequently, it describes policies that select actions based only on their short-term consequences. The beauty of $v_*$ is that if one uses it to evaluate the short-term consequences of actions—specifically, the one-step consequences—then a greedy policy is actually optimal in the long-term sense in which we are interested because $v_*$ already takes into account the reward consequences of all possible future behavior. By means of $v_*$, the optimal expected long-term return is turned into a quantity that is locally and immediately available for each state. Hence, a one-step-ahead search yields the long-term optimal actions."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:reinforcement_learning] *",
   "language": "python",
   "name": "conda-env-reinforcement_learning-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
